1. Control de versiones (VC)
    sistema de gestion de versiones (permite tener registro de todos los cambios y generar respaldos del proyecto)
2. Control de versiones distribuido (DVC)
    permite compartir las versiones del proyecto sin nececidad de una red (conecciones peer-to-peer)
3. Repositorio remoto y Repositorio local
    el repositorio remoto  son las versiones que se encuentran en la nube mientras que el local solo se encuentra en la computadora (descargada) 
4. Copia de trabajo / Working Copy
    proceso de copia del repositorio remoto al local
5. Área de Preparación / Staging Area
    enlistado de todos los archivos a cambiar antes del commit
6. Preparar Cambios / Stage Changes
    lineas de codigo a cambiar en el proximo commit
7. Confirmar cambios / Commit Changes
    proceso de aceptar los cambios realizados en el proyecto y estos seran subidos repositorio remoto
8. Commit
    permite agregar cambios realizados en el HEAD
9. clone
    genera una copia local del respositorio solicitado
10. pull
    actualiza los archivos al commit actual
11. push
    sube todo lo que se encuentre en el heado y lo sube al repositorio remoto
12. fetch
    borra el repositorio local y descarga nuevamente los del remoto
13. merge
    para funsionar ramas activas
14. status
    muestra las diferencias del index y el actual head (ademas del repositorio local)
16. log
    permite ver el historial de commits que se han realizados
17. checkout
    permite regresar a la version estable contenida en el head y corrige errores antes del index de manera local 
18. Rama / Branch
    genera una rama (version paralela a la master) con funcionalidad especifica 
19. Etiqueta / Tag
    hace referencia a puntos especificos en las versiones del repositorio

## fuente 
    https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas
    https://git-scm.com/book/es/v2/Appendix-B%3A-Comandos-de-Git-Seguimiento-B%C3%A1sico
    http://rogerdudler.github.io/git-guide/index.es.html
    https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones
    https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos
    https://networkfaculty.com/es/video/detail/2604-git---area-de-preparacion-o-indice---standing-area
    https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio
